from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import render, redirect
from .forms import FormStatus
from .models import Activity
from django.http import HttpResponseRedirect

response = {}
# Create your views here.
def landing_page_add_status(request):
	form = FormStatus(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
	    response['message'] = request.POST['message']
	    activity = Activity(message = response['message'])
	    activity.save()
	    return HttpResponseRedirect('/landingpage')
	else:
	    return HttpResponseRedirect('/landingpage')

	return render(request, 'landingpage.html', response)

def landing_page(request):
	statusq = Activity.objects.all()
	form = FormStatus()
	return render(request, 'landingpage.html', {'form': form, 'statusq': statusq})

def profile(request):
	response = {}
	return render(request, 'profilemela.html', response)