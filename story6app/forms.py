from django import forms


class FormStatus(forms.Form):
	status_attrs = {
	    'type':'text',
	    'class':'form-control',
	    'placeholder':'Your Mood',
	}
	message = forms.CharField(max_length = 800, widget = forms.TextInput(attrs = status_attrs), label = "What's Your Mood?")
