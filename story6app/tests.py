from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import landing_page, landing_page_add_status, profile
from .models import Activity
from .forms import FormStatus
##import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Story6(TestCase):
	# Create your tests here.
	def test_story_6_url_is_exist(self):
		response = Client().get('/landingpage')
		self.assertEqual(response.status_code, 200)

	def test_story_6_using_index_func(self):
		found = resolve('/landingpage')
		self.assertEqual(found.func, landing_page)

	def test_story_6_add_using_index_func(self):
		found = resolve('/landingpage_add')
		self.assertEqual(found.func, landing_page_add_status)

	def test_using_status_model(self):
		Activity.objects.create(message='Hello')
		jumlahObjek = Activity.objects.all().count()
		self.assertEqual(jumlahObjek, 1)

class Story6_Profile(TestCase):

	def test_story_6_url_is_exist(self):
		response = Client().get('/profile')
		self.assertEqual(response.status_code, 200)

	def test_story_6_using_index_func(self):
		found = resolve('/profile')
		self.assertEqual(found.func, profile)

class Story6FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://mela-ppwe.herokuapp.com/landingpage')
        # find the form element
        title = selenium.find_element_by_class_name("form-control")

        submit = selenium.find_element_by_tag_name('button')

        # Fill the form with data
        title.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        self.assertIn('Coba Coba', selenium.page_source)

    def test_background_is_gradient(self):
        selenium = self.selenium
        selenium.get('http://mela-ppwe.herokuapp.com/landingpage')
        body = selenium.find_element_by_tag_name('body').value_of_css_property('background-image')
        self.assertEqual(body, "linear-gradient(to left top, rgb(132, 94, 194), rgb(0, 133, 228), rgb(0, 162, 229), rgb(0, 185, 204), rgb(0, 201, 167))")

    def test_font(self):
        selenium = self.selenium
        selenium.get('http://mela-ppwe.herokuapp.com/landingpage')
        font = selenium.find_element_by_tag_name('h1').value_of_css_property('font-family')
        self.assertIn("Montserrat", font)

    def test_location_status_box(self):
        selenium = self.selenium
        selenium.get('http://mela-ppwe.herokuapp.com/landingpage')
        locationbox = selenium.find_element_by_class_name('form-control')

    def test_location_submit_button(self):
        selenium = self.selenium
        selenium.get('http://mela-ppwe.herokuapp.com/landingpage')
        locationsubmit = selenium.find_element_by_tag_name('button')
