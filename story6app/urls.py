from django.urls import path
from . import views

app_name ="story6app"

urlpatterns = [
    path('landingpage', views.landing_page, name="landingpage"),
    path('landingpage_add', views.landing_page_add_status, name="landingpage_add"),
    path('profile', views.profile, name="profile")
]