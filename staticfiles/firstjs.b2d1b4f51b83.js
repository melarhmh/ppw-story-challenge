$(document).ready(function() {
	setTimeout(function() {
    $('#loader').fadeOut('slow');
    }, 1500);
	$("#daymode").click(function(){
		if($("body").css("background-color") == "rgb(7, 38, 148)") {
			$("body").css("background-color", "#48B5E5");
			$(".accordion").css("background-color", "#9EE1FF");
			$(".accordion").css("color", "#000000");
			$(".panel").css("color", "#0D93CF");
			$("#daymode").removeClass("btn-dark");
			$("#daymode").addClass("btn-light");

		} else {
			$("body").css("background-color", "#072694");
			$(".accordion").css("background-color", "#4C6BDC");
			$(".accordion").css("color", "#FFFFFF");
			$(".panel").css("color", "#072694");
			$("#daymode").removeClass("btn-light");
			$("#daymode").addClass("btn-dark");
		}
	}); 

	
	var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
  }
});
