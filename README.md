* Nama				: Khameela Rahmah
* NPM				: 1706023422
* Kelas				: PPW - E
* Link Landing Page	: http://mela-ppwe.herokuapp.com/landingpage
* Link Profile		: http://mela-ppwe.herokuapp.com/profile

## Status
[![Pipeline](https://gitlab.com/melarhmh/ppw-story-challenge/badges/master/pipeline.svg)](https://gitlab.com/melarhmh/ppw-story-challenge/commits/master)
[![Coverage](https://gitlab.com/melarhmh/ppw-story-challenge/badges/master/coverage.svg)](https://gitlab.com/melarhmh/ppw-story-challenge/commits/master)